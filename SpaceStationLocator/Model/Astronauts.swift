//
//  Astronauts.swift
//  SpaceStationLocator
//
//  Created by Christine Chang on 3/22/23.
//

import Foundation

struct Astronauts: Codable {
    let message: String
    let number: Int
    let people: [Person]
}

struct Person: Codable {
    let craft: String
    let name: String
}
