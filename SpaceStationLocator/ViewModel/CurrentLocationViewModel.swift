//
//  CurrentLocationViewModel.swift
//  SpaceStationLocator
//
//  Created by Christine Chang on 3/22/23.
//

import Foundation
import UIKit
import CoreLocation
import CoreData

class CurrentLocationViewModel: NSObject {
    
    // TODO: Use an enum to construct the endpoints in a more modular fashion
    private let issUrl = "http://api.open-notify.org/iss-now.json"
    private let astronautsUrl = "http://api.open-notify.org/astros.json"
    private let locationManager = LocationManager()
    private var userLocation: CLLocation? = nil
    private var appDelegate = UIApplication.shared.delegate as? AppDelegate
    private(set) var milesFromISS: Int? = nil
    private(set) var astronauts = [String]()
    
    override init() {
        super.init()
        locationManager.delegate = self
    }
    
    func getISSLocation(completion: @escaping () -> Void) {
        ISSNetworkClient().makeRequest(url: issUrl) { [weak self] (result: Result<ISSStatus, Error>) in
            switch result {
            case .success(let issStatus):
                guard let self = self else {
                    return
                }

                // Calculate user's distance to ISS
                if let issLocation = self.locationManager.getLocationFor(issStatus: issStatus),
                   let userLocation = self.userLocation {
                    let numMiles = self.locationManager.getMilesBetween(issLocation, userLocation)
                    self.milesFromISS = Int(numMiles)
                }
                
                // Store ISS locations in Core Data
                let issLat = Double(issStatus.iss_position.latitude) ?? 0.0
                let issLong = Double(issStatus.iss_position.longitude) ?? 0.0
                let time = Int32(issStatus.timestamp)
                self.saveCoordinates(latitude: issLat, longitude: issLong, timestamp: time)
                completion()

            case .failure(let error):
                switch error {
                case NetworkError.invalidURL:
                    print("Invalid URL")
                    completion()
                case NetworkError.requestFailed(let statusCode):
                    print("Request failed with status code: \(statusCode)")
                    // TODO: If statusCode is 5xx, then retry
                    completion()
                case NetworkError.noData:
                    print("No data found")
                    completion()
                case NetworkError.invalidData:
                    print("Invalid data")
                    completion()
                default:
                    print("Request failed with error: \(error.localizedDescription)")
                    completion()
                }
            }
        }
    }
    
    func getISSAstronauts(completion: @escaping () -> Void) {
        ISSNetworkClient().makeRequest(url: astronautsUrl) { [weak self] (result: Result<Astronauts, Error>) in
            switch result {
            case .success(let astronauts):
                guard let self = self else {
                    return
                }
                
                self.astronauts = astronauts.people.map { person in person.name }
                completion()

            case .failure(let error):
                switch error {
                case NetworkError.invalidURL:
                    print("Invalid URL")
                    completion()
                case NetworkError.requestFailed(let statusCode):
                    print("Request failed with status code: \(statusCode)")
                    // TODO: If statusCode is 5xx, then retry
                    completion()
                case NetworkError.noData:
                    print("No data found")
                    completion()
                case NetworkError.invalidData:
                    print("Invalid data")
                    completion()
                default:
                    print("Request failed with error: \(error.localizedDescription)")
                    completion()
                }
            }
        }
    }
}

// MARK: - Location Delegate
extension CurrentLocationViewModel: LocationManagerDelegate {
    func updateUserLocation(latitude: CLLocationDegrees, longitude: CLLocationDegrees) {
        userLocation = CLLocation(latitude: latitude, longitude: longitude)
    }
}

// MARK: - Core Data
extension CurrentLocationViewModel {
    private func saveCoordinates(latitude: Double, longitude: Double, timestamp: Int32) {
        guard let managedContext = appDelegate?.persistentContainer.viewContext else {
            print("Failed to retrieve managedContext 😞")
            return
        }

        let entity = NSEntityDescription.entity(forEntityName: "ISSLocation", in: managedContext)!
        let location = NSManagedObject(entity: entity, insertInto: managedContext)

        location.setValue(latitude, forKeyPath: "latitude")
        location.setValue(longitude, forKeyPath: "longitude")
        location.setValue(timestamp, forKeyPath: "timestamp")

        do {
            try managedContext.save()
            print("Location saved: \(location) 🎉")
        } catch let error as NSError {
            print("Failed to save data. \(error), \(error.userInfo)")
        }
    }
}
