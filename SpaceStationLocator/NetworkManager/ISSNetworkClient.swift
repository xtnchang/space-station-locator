//
//  ISSNetworkClient.swift
//  SpaceStationLocator
//
//  Created by Christine Chang on 3/22/23.
//

import Foundation

enum NetworkError: Error {
    case invalidURL, requestFailed(_ statusCode: Int), noData, invalidData
}

class ISSNetworkClient {
    func makeRequest<T: Codable>(url: String, completion: @escaping (Result<T, Error>) -> Void) {
        guard let url = URL(string: url) else {
            completion(.failure(NetworkError.invalidURL))
            return
        }
        let request = URLRequest(url: url)
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            if let error = error {
                completion(.failure(error))
                return
            }
            
            if let statusCode = (response as? HTTPURLResponse)?.statusCode, !(200...299).contains(statusCode) {
                completion(.failure(NetworkError.requestFailed(statusCode)))
                return
            }
            
            guard let positionData = data else {
                completion(.failure(NetworkError.noData))
                return
            }
            
            do {
                let position = try JSONDecoder().decode(T.self, from: positionData)
                completion(.success(position)) // All checks passed; success!
            } catch {
                completion(.failure(NetworkError.invalidData))
                return
            }
        }
        task.resume()
    }
}
