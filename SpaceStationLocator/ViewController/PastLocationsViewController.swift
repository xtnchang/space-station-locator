//
//  PastLocationViewController.swift
//  SpaceStationLocator
//
//  Created by Christine Chang on 3/22/23.
//

import UIKit
import CoreData

class PastLocationsViewController: UIViewController {
    
    private let pastLocationsTableView = UITableView()
    private let locationCellIdentifier = "locationCell"
    private let viewModel = PastLocationsViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        constructTableView()
        setPastLocationsTableViewConstraints()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.fetchLocationData()
        pastLocationsTableView.reloadData() // Reload the table view after a fresh fetch from Core Data
    }
    
    private func constructTableView() {
        pastLocationsTableView.dataSource = self
        pastLocationsTableView.delegate = self
        pastLocationsTableView.register(UITableViewCell.self, forCellReuseIdentifier: locationCellIdentifier)
        view.addSubview(pastLocationsTableView)
    }
    
    private func setPastLocationsTableViewConstraints() {
        NSLayoutConstraint.activate([
            pastLocationsTableView.topAnchor.constraint(equalTo: view.topAnchor, constant: 10),
            pastLocationsTableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            pastLocationsTableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            pastLocationsTableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
        pastLocationsTableView.translatesAutoresizingMaskIntoConstraints = false
    }
}

// MARK: - Table View Delegate Methods
extension PastLocationsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.locations.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var locationString = "Missing location data"
        var timeString = "Missing time data"
        let location = viewModel.locations[safe: indexPath.row]
        if let latitude = location?.value(forKeyPath: "latitude") as? Double,
           let longitude = location?.value(forKeyPath: "longitude") as? Double,
           let timestamp = location?.value(forKeyPath: "timestamp") as? Int {
            locationString = "Latitude: \(latitude), Longitude: \(longitude)"
            timeString = DateUtils.getReadableDateForUnixTime(timestamp)
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: locationCellIdentifier, for: indexPath)
        var content = cell.defaultContentConfiguration()
        content.text = locationString
        content.secondaryText = timeString

        cell.contentConfiguration = content
        cell.selectionStyle = .none

        return cell
    }
}
