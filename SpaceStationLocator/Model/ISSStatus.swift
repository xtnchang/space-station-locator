//
//  ISSStatus.swift
//  SpaceStationLocator
//
//  Created by Christine Chang on 3/22/23.
//

import Foundation

struct ISSStatus: Codable {
    let iss_position: Coordinate
    let timestamp: Int
    let message: String
}

struct Coordinate: Codable {
    let latitude: String
    let longitude: String
}
