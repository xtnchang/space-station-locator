//
//  PastLocationsViewModel.swift
//  SpaceStationLocator
//
//  Created by Christine Chang on 3/22/23.
//

import Foundation
import UIKit
import CoreData

class PastLocationsViewModel {

    private(set) var locations: [NSManagedObject] = []
    
    func fetchLocationData() {
        locations = CoreDataService.shared.fetchLocationData()
    }
}
