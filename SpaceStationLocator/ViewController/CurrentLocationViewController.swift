//
//  ViewController.swift
//  SpaceStationLocator
//
//  Created by Christine Chang on 3/22/23.
//

import UIKit
import CoreLocation

class CurrentLocationViewController: UIViewController {
    
    private let viewModel = CurrentLocationViewModel()
    private let issDistanceLabel = UILabel()
    private let astronautsAboardLabel = UILabel()
    private let astronautsTableView = UITableView()
    private let astronautCellIdentifier = "astronautCell"
    private var timer: Timer?

    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        timer?.invalidate()
    }
    
    private func fetchData() {
        // Get the ISS location data
        getISSLocation()

        // Continue fetching ISS location data at 5 second intervals
        timer = Timer.scheduledTimer(timeInterval: 5.0,
                                     target: self,
                                     selector: #selector(getISSLocation),
                                     userInfo: nil,
                                     repeats: true)
        
        // Get the astronaut data
        viewModel.getISSAstronauts() { [weak self] in
            DispatchQueue.main.async {
                self?.astronautsTableView.reloadData()
            }
        }
    }
    
    @objc private func getISSLocation() {
        viewModel.getISSLocation() { [weak self] in
            DispatchQueue.main.async {
                self?.constructISSDistanceLabel()
            }
        }
    }
    
    private func configureUI() {
        view.backgroundColor = .white
        constructISSDistanceLabel()
        constructAstronautsAboardLabel()
        constructTableView()
        setISSDistanceLabelConstraints()
        setAstronautsAboardLabelConstraints()
        setAstronautsTableViewConstraints()
    }
    
    private func constructISSDistanceLabel() {
        if let numMiles = viewModel.milesFromISS {
            issDistanceLabel.text = "You are currently \(numMiles) miles away from the ISS!"
        } else {
            issDistanceLabel.text = "Your distance from the ISS is currently unavailabe. Be sure to enable location permissions!"
        }
        issDistanceLabel.numberOfLines = 0
        view.addSubview(issDistanceLabel)
    }
    
    private func constructAstronautsAboardLabel() {
        astronautsAboardLabel.text = "Astronauts currently aboard the ISS:"
        astronautsAboardLabel.numberOfLines = 0
        view.addSubview(astronautsAboardLabel)
    }
    
    private func constructTableView() {
        astronautsTableView.dataSource = self
        astronautsTableView.delegate = self
        astronautsTableView.register(UITableViewCell.self, forCellReuseIdentifier: astronautCellIdentifier)
        view.addSubview(astronautsTableView)
    }
    
    private func setISSDistanceLabelConstraints() {
        let maxWidth: CGFloat = view.frame.width - 80

        NSLayoutConstraint.activate([
            issDistanceLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: view.frame.height * 0.1),
            issDistanceLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 40),
            issDistanceLabel.trailingAnchor.constraint(lessThanOrEqualTo: view.trailingAnchor, constant: 40),
            issDistanceLabel.widthAnchor.constraint(lessThanOrEqualToConstant: maxWidth)
        ])
        
        issDistanceLabel.translatesAutoresizingMaskIntoConstraints = false
    }
    
    private func setAstronautsAboardLabelConstraints() {
        let maxWidth: CGFloat = view.frame.width - 80

        NSLayoutConstraint.activate([
            astronautsAboardLabel.topAnchor.constraint(equalTo: issDistanceLabel.bottomAnchor, constant: 20),
            astronautsAboardLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 40),
            astronautsAboardLabel.trailingAnchor.constraint(lessThanOrEqualTo: view.trailingAnchor, constant: 40),
            astronautsAboardLabel.widthAnchor.constraint(lessThanOrEqualToConstant: maxWidth)
        ])
        
        astronautsAboardLabel.translatesAutoresizingMaskIntoConstraints = false
    }
    
    private func setAstronautsTableViewConstraints() {
        NSLayoutConstraint.activate([
            astronautsTableView.topAnchor.constraint(equalTo: astronautsAboardLabel.bottomAnchor, constant: 10),
            astronautsTableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            astronautsTableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            astronautsTableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
        astronautsTableView.translatesAutoresizingMaskIntoConstraints = false
    }
}

// MARK: - Table View Delegate Methods
extension CurrentLocationViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.astronauts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: astronautCellIdentifier, for: indexPath)
        var content = cell.defaultContentConfiguration()
        content.text = viewModel.astronauts[safe: indexPath.row]
        cell.contentConfiguration = content
        cell.selectionStyle = .none
        
        return cell
    }
}
