//
//  DateUtils.swift
//  SpaceStationLocator
//
//  Created by Christine Chang on 3/23/23.
//

import Foundation

struct DateUtils {
    static func getReadableDateForUnixTime(_ timestamp: Int) -> String {
        let date = Date(timeIntervalSince1970: TimeInterval(timestamp))
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd, yyyy, h:mm a"
        dateFormatter.timeZone = TimeZone(abbreviation: "EST")
        let dateString = dateFormatter.string(from: date)
        return String(dateString)
    }
}
