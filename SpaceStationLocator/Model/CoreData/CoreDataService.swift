//
//  CoreDataService.swift
//  SpaceStationLocator
//
//  Created by Christine Chang on 3/23/23.
//

import Foundation
import UIKit
import CoreData

class CoreDataService {
    static let shared = CoreDataService()
    var appDelegate = UIApplication.shared.delegate as? AppDelegate
    
    func getManagedObjectContext() -> NSManagedObjectContext? {
        guard let managedContext = appDelegate?.persistentContainer.viewContext else {
            print("Failed to retrieve managedContext 😞")
            return nil
        }
        
        return managedContext
    }
    
    func fetchDataFromManagedObjectContext(fetchRequest: NSFetchRequest<NSManagedObject>) -> [NSManagedObject] {
        if let managedContext = getManagedObjectContext() {
            do {
                let fetchedData = try managedContext.fetch(fetchRequest)
                return fetchedData
            } catch let error as NSError {
                print("Failed to fetch data. \(error), \(error.userInfo)")
            }
        }

        return []
    }
    
    func fetchLocationData() -> [NSManagedObject] {
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "ISSLocation")
        return fetchDataFromManagedObjectContext(fetchRequest: fetchRequest)
    }
}
