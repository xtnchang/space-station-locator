//
//  DateUtilsTests.swift
//  SpaceStationLocatorTests
//
//  Created by Christine Chang on 3/23/23.
//

import XCTest
@testable import SpaceStationLocator

final class DateUtilsTests: XCTestCase {

    func testGetReadableDateForUnixTime() throws {
        // Test with a timestamp representing 12:00 AM on Jan 1, 2023 EST
        let timestamp = 1672549200
        var readableDate = DateUtils.getReadableDateForUnixTime(timestamp)
        let expectedDate = "Jan 01, 2023, 12:00 AM"
        let expectedDateMissingComma = "Jan 01, 2023 12:00 AM"
        let expectedDateNumericalMonth = "01 01, 2023, 12:00 AM"
        let expectedDateLowercasePM = "Jan 01, 2023, 12:00 am"
        XCTAssertEqual(readableDate, expectedDate)
        XCTAssertNotEqual(readableDate, expectedDateMissingComma)
        XCTAssertNotEqual(readableDate, expectedDateNumericalMonth)
        XCTAssertNotEqual(readableDate, expectedDateLowercasePM)

        // Test with a negative timestamp
        let negativeTimestamp = -1000000000
        let expectedDateForNegTimestamp = "Apr 24, 1938, 6:13 PM"
        readableDate = DateUtils.getReadableDateForUnixTime(negativeTimestamp)
        XCTAssertEqual(readableDate, expectedDateForNegTimestamp)
    }
}
