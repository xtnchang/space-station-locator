//
//  LocationManager.swift
//  SpaceStationLocator
//
//  Created by Christine Chang on 3/22/23.
//

import Foundation
import CoreLocation

// Delegate the task/implementation of updating user location to CurrentLocationViewModel
protocol LocationManagerDelegate: AnyObject {
    func updateUserLocation(latitude: CLLocationDegrees, longitude: CLLocationDegrees)
}

class LocationManager: NSObject, CLLocationManagerDelegate {

    private let locationManager = CLLocationManager()
    weak var delegate: LocationManagerDelegate?
    
    override init() {
        super.init()
        locationManager.delegate = self
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .authorizedAlways, .authorizedWhenInUse:
            locationManager.startUpdatingLocation()
        case .denied, .restricted:
            locationManager.stopUpdatingLocation()
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
        default:
            break
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            let latitude = location.coordinate.latitude
            let longitude = location.coordinate.longitude
            delegate?.updateUserLocation(latitude: latitude, longitude: longitude)
        }
    }
    
    func getLocationFor(issStatus: ISSStatus) -> CLLocation? {
        guard let latDouble = Double(issStatus.iss_position.latitude) else {
            return nil
        }
        guard let longDouble = Double(issStatus.iss_position.longitude) else {
            return nil
        }
        
        return CLLocation(latitude: latDouble, longitude: longDouble)
    }
    
    func getMilesBetween(_ location1: CLLocation, _ location2: CLLocation) -> Double {
        let distanceInMeters = location1.distance(from: location2)
        let metersToConvert = Measurement(value: distanceInMeters, unit: UnitLength.meters)
        let miles = metersToConvert.converted(to: UnitLength.miles).value.rounded()
        return miles
    }
}
