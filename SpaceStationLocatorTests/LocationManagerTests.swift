//
//  LocationManagerTests.swift
//  SpaceStationLocatorTests
//
//  Created by Christine Chang on 3/23/23.
//

import XCTest
import CoreLocation
@testable import SpaceStationLocator

final class LocationManagerTests: XCTestCase {
    
    private let locationManager = LocationManager()

    func testGetLocationForValidCoordinate() {
        let coordinate = Coordinate(latitude: "51.5072", longitude: "0.1276")
        let issStatus = ISSStatus(iss_position: coordinate, timestamp: 1648300861, message: "success")
        let expectedLocation = CLLocation(latitude: 51.5072, longitude: 0.1276)
        let actualLocation = locationManager.getLocationFor(issStatus: issStatus)
        XCTAssertEqual(actualLocation?.coordinate.latitude, expectedLocation.coordinate.latitude)
        XCTAssertEqual(actualLocation?.coordinate.longitude, expectedLocation.coordinate.longitude)
    }
    
    func testGetLocationForInvalidCoordinate() {
        let coordinate = Coordinate(latitude: "invalid", longitude: "0.1276")
        let issStatus = ISSStatus(iss_position: coordinate, timestamp: 1648300861, message: "success")
        let actualLocation = locationManager.getLocationFor(issStatus: issStatus)
        XCTAssertNil(actualLocation)
    }
    
    func testGetLocationForMissingCoordinate() {
        let issStatus = ISSStatus(iss_position: Coordinate(latitude: "", longitude: ""), timestamp: 1648300861, message: "success")
        let actualLocation = locationManager.getLocationFor(issStatus: issStatus)
        XCTAssertNil(actualLocation)
    }
    
    func testGetMilesBetweenLocations() {
        let location1 = CLLocation(latitude: 37.2952794, longitude: -122.0041785)
        let location2 = CLLocation(latitude: 38.5382322, longitude: -121.7639065)
        let expectedMiles = 87.0
        let actualMiles = locationManager.getMilesBetween(location1, location2)
        XCTAssertEqual(actualMiles, expectedMiles)
    }
    
    func testGetMilesBetweenSameLocation() {
        let location = CLLocation(latitude: 37.7749, longitude: -122.4194)
        let expectedMiles = 0.0
        let actualMiles = locationManager.getMilesBetween(location, location)
        XCTAssertEqual(actualMiles, expectedMiles)
    }
}
