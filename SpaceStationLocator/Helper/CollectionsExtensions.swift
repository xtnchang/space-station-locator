//
//  CollectionsExtensions.swift
//  SpaceStationLocator
//
//  Created by Christine Chang on 3/23/23.
//

import Foundation

extension Collection {
    subscript(safe index: Index) -> Element? {
        return indices.contains(index) ? self[index] : nil
    }
}
