//
//  TabBarController.swift
//  SpaceStationLocator
//
//  Created by Christine Chang on 3/22/23.
//

import UIKit

class TabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let firstViewController = CurrentLocationViewController()
        firstViewController.tabBarItem = UITabBarItem(title: "Home", image: UIImage(systemName: "house"), tag: 0)

        let secondViewController = PastLocationsViewController()
        secondViewController.tabBarItem = UITabBarItem(title: "History", image: UIImage(systemName: "clock.arrow.circlepath"), tag: 0)

        self.viewControllers = [firstViewController, secondViewController]

        tabBar.backgroundColor = .systemGray6
    }
}
